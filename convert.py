import csv
import json

with open('input.csv', 'r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    data = list(csv_reader)

with open('output.json', 'w') as json_file:
    json.dump(data, json_file)
